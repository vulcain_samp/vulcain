from django.apps import AppConfig


class VulcainMainConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "Vulcain_main"
